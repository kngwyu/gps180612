(require 'org)
(require 'ox-publish)
(add-to-list 'load-path "./org-reveal")
(require 'ox-reveal)

(setq org-export-with-smart-quotes t
      org-confirm-babel-evaluate nil)


(setq org-publish-project-alist
      (list
       (list "slide"
	     :base-directory "."
	     :base-extension "org"
	     :publishing-directory "./public"
             :publishing-function 'org-reveal-publish-to-reveal)
       (list "theme"
	     :base-directory "."
	     :base-extension  "css"
	     :publishing-directory "./public/"
	     :publishing-function 'org-publish-attachment)
       (list "pictures"
	     :base-directory "."
	     :base-extension  "png"
	     :publishing-directory "./public/"
	     :publishing-function 'org-publish-attachment)
       (list "site" :components '("slide"))))

(provide 'publish)
