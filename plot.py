import numpy as np
import matplotlib.pyplot as plt

mu, sigma = 0.5, 0.1
x = mu + sigma * np.random.randn(10000)

fig = plt.figure()
ax = fig.add_subplot(211)
ax.hist(x, bins=10)
ax.set_xlabel("Probability")
ax.set_ylabel("Value")
plt.show()
